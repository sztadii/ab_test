# Little info about project:
* Directory './components' contains all reusable components.
* Directory './helpers' contains scripts helping in develop, sort etc.
* Directory './objects' contain global object which are dependencies of other object, services, etc.
* Directory './services' contains services to fetch, get some date like timeTables.
* Directory './styles' contains styles written in BEM.
* Directory './test' contains whole test written in Mocha.