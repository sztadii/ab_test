Vue.component('clock', {
  data() {
    return globals;
  },
  computed: {
    time() {
      return minutesToTime(this.minutes);
    }
  },
  template: `
    <div class="clock">
      {{time}}
    </div>
  `
})