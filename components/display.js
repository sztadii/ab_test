Vue.component('display', {
  data() {
    return globals;
  },
  created() {
    timeTable.init();
  },
  methods: {
    adjustTime(minutes) {
      return minutes - this.minutes;
    },
    isCancel(cancel) {
      return cancel ? "Canceled" : "";
    }
  },
  template: `
  <div class="display">
    <div v-if="index" v-for="n in 3" class="display__item">
      <div class="display__destiny">{{index + n}} {{array[index + n].destiny}}</div>
      <div class="display__time">{{isCancel(array[index + n].cancel)}}
        <span v-if="!array[index + n].cancel">{{adjustTime(array[index + n].minutes)}} min</span>
      </div>
    </div>
  </div>`
})