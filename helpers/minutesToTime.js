function minutesToTime (_minutes) {
  var hours = Math.floor(_minutes / 60);
  var minutes = _minutes % 60;

  while(hours >= 24) hours = hours - 24;

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;

  return hours + ":" + minutes;
}