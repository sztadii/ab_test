function sortBy (col, iterator) {

  var isString = typeof iterator === 'string',
    len = col.length,
    temp, lowest;

  col.map(function(_, index) {
    lowest = index;
    if (isString) {
      for (var i = index + 1; i < len; i++) {
        if (col[i][iterator] < col[lowest][iterator])
          lowest = i;
      }
    } else {
      for (var i = index + 1; i < len; i++) {
        if (iterator(col[i]) < iterator(col[lowest]) || col[lowest] === undefined)
          lowest = i;
      }
    }
    if (index != lowest) {
      temp = col[index];
      col[index] = col[lowest];
      col[lowest] = temp;
    }
  });

  return col;
}