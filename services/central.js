var central = new function () {

  //private vars
  var name = "Central Station";
  var array = [];
  var minutes = 0;
  var duration = 20;
  var finish = 1440;

  //public methods
  function get() {
    init();
    return array;
  }

  //private methods
  function init() {
    var i = 0;

    while(minutes <= finish) {
      array.push({
        destiny: name,
        minutes: minutes,
        cancel: thirdCancel(i)
      });

      minutes += duration;
      i++;
    }
  }

  function thirdCancel(i) {
    return (i + 1) % 3 == 0 ? true : false;
  }

  return {
    get: get
  }
}