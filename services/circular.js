var circular = new function () {

  //private vars
  var name = "Circular";
  var array = [];
  var minutes = 0;
  var duration = 60;
  var finish = 1440;

  //public methods
  function get() {
    init();
    return array;
  }

  //private methods
  function init() {
    while(minutes <= finish) {
      array.push({
        destiny: name,
        minutes: minutes,
        cancel: false
      });

      minutes += duration;
    }
  }

  return {
    get: get
  }
}