var counter = new function () {

  //bind events
  window.addEventListener('load', load);

  function load () {
    setInterval(() => {
      var isIndex = findFirstIndex(globals.array, globals.minutes);
      if(isIndex) globals.index = isIndex;
      globals.minutes++;
    }, 300)
  }
}