var north = new function () {

  //private vars
  var name = "North Square";
  var array = [];
  var minutes = 420;
  var duration = 12;
  var finish = 1320;

  //public methods
  function get() {
    init();
    return array;
  }

  //private methods
  function init() {
    while(minutes <= finish) {
      array.push({
        destiny: name,
        minutes: minutes,
        cancel: false
      });

      minutes += duration;
    }
  }

  return {
    get: get
  }
}