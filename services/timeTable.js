var timeTable = new function () {

  //public methods
  function init() {
    globals.array = globals.array.concat(central.get(), circular.get(), north.get(), west.get());
    globals.array = sortBy(globals.array, 'minutes');
  }

  return {
    init: init
  }
}