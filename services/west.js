var west = new function () {

  //private vars
  var name = "West Market";
  var array = [];
  var minutes = 330;
  var duration = 6;
  var finish = 1530;

  //public methods
  function get() {
    init();
    return array;
  }

  //private methods
  function init() {
    while(minutes <= finish) {
      array.push({
        destiny: name,
        minutes: minutes,
        cancel: false
      });

      minutes += duration;
    }
  }

  return {
    get: get
  }
}