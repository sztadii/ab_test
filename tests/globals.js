//ONE TEST, BUT WE CHECK IS EXISTS, IS ARRAY, IS NOT NULL, HAVE LENGTH AND HAVE THIS PROPERTIES
//IF WE CAN WRITE LESS TEST WE SHOULD, BUT TEST MUST MATCH ALL APP
describe('Globals array object', function() {
  it('it expect have property destiny, minutes, cancel', function() {
    expect(globals.array[0]).to.contain.all.keys(['destiny', 'minutes', 'cancel']);
  });

  it('it expect property destiny have length', function() {
    expect(globals.array[0].destiny).to.have.length.above(0);
  });

  it('it expect property minutes is integer', function() {
    expect(globals.array[0].minutes).to.satisfy(Number.isInteger);
  });

  it('it expect every third central station to be canceled', function() {
    var array = central.get();
    expect(array[0].cancel).to.be.false;
    expect(array[1].cancel).to.be.false;
    expect(array[2].cancel).to.be.true;
    expect(array[5].cancel).to.be.true;
  });
});


